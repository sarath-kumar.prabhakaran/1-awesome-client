package com.network.rest;

public class ProviderBean {
	
	private String name;
	private AccessType unlimited;
	private int speed;
	private int hotspot;
	private AccessType datarollover;
	private AccessType hdstreaming;
	private AccessType earlytermination;
	private AccessType business;
	private int international;
	
	public ProviderBean() {
		super();
	}
	
	public ProviderBean(String name,AccessType unlimited, int speed, int hotspot, AccessType datarollover, AccessType hdstreaming,
			AccessType earlytermination, AccessType business, int international) {
		super();
		this.setName(name);
		this.setUnlimited(unlimited);
		this.setSpeed(speed);
		this.setHotspot(hotspot);
		this.setDatarollover(datarollover);
		this.setHdstreaming(hdstreaming);
		this.setEarlytermination(earlytermination);
		this.setBusiness(business);
		this.setInternational(international);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public AccessType getUnlimited() {
		return unlimited;
	}

	public void setUnlimited(AccessType unlimited) {
		this.unlimited = unlimited;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getHotspot() {
		return hotspot;
	}

	public void setHotspot(int hotspot) {
		this.hotspot = hotspot;
	}

	public AccessType getDatarollover() {
		return datarollover;
	}

	public void setDatarollover(AccessType datarollover) {
		this.datarollover = datarollover;
	}

	public AccessType getHdstreaming() {
		return hdstreaming;
	}

	public void setHdstreaming(AccessType hdstreaming) {
		this.hdstreaming = hdstreaming;
	}

	public AccessType getEarlytermination() {
		return earlytermination;
	}

	public void setEarlytermination(AccessType earlytermination) {
		this.earlytermination = earlytermination;
	}

	public AccessType getBusiness() {
		return business;
	}

	public void setBusiness(AccessType business) {
		this.business = business;
	}

	public int getInternational() {
		return international;
	}

	public void setInternational(int international) {
		this.international = international;
	}
	
	
}
