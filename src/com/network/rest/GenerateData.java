package com.network.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;

public class GenerateData {
	
	private static List<ProviderBean> allnetwork;
	private static Map<String,ProviderBean> networkproviders;
	
	static {	
		
		allnetwork = new ArrayList<ProviderBean>();
		networkproviders = new HashMap<String,ProviderBean>();
		
		allnetwork.add(new ProviderBean("sprint",AccessType.AVAILABLE,23,10,AccessType.NOTAVAILABLE,AccessType.AVAILABLE,AccessType.AVAILABLE,AccessType.AVAILABLE,15));
		allnetwork.add(new ProviderBean("verizon",AccessType.AVAILABLE,22,10,AccessType.AVAILABLE,AccessType.AVAILABLE,AccessType.AVAILABLE,AccessType.AVAILABLE,15));
		allnetwork.add(new ProviderBean("tmobile",AccessType.AVAILABLE,30,10,AccessType.AVAILABLE,AccessType.AVAILABLE,AccessType.NOTAVAILABLE,AccessType.AVAILABLE,15));
		allnetwork.add(new ProviderBean("at&t",AccessType.AVAILABLE,22,10,AccessType.NOTAVAILABLE,AccessType.AVAILABLE,AccessType.NOTAVAILABLE,AccessType.AVAILABLE,40));
		
		for(ProviderBean provider : allnetwork) {
			networkproviders.put(provider.getName(), provider);
		}
		
	}
	
	public String getNetworkJSON() {
		String json = new Gson().toJson(networkproviders);
		return json;
	}
	
	public String getNetwork(String network) {
		String keyData = network.toLowerCase();
		ProviderBean returnDay = networkproviders.get(keyData);
		if(returnDay == null) {
			return "Sorry Bad Input";
		}
		return new Gson().toJson(returnDay);
	}
	
	public String getFilter(int filterindex) {
		
		Map<String,Integer> highSpeed;
		Map<String,Integer> Hotspot;
		Map<String,AccessType> Rollover;
		Map<String,AccessType> Termination;
		Map<String,Integer> International;
		
		 if(filterindex == 0) {
            	highSpeed = new HashMap<String,Integer>();
                
                for(ProviderBean bean : allnetwork) {
                	highSpeed.put(bean.getName().toUpperCase(), bean.getSpeed());
                }
                
                return new Gson().toJson(highSpeed);
		 }else if(filterindex == 1) {
            	Hotspot = new HashMap<String,Integer>();
                
                for(ProviderBean bean : allnetwork) {
                	Hotspot.put(bean.getName().toUpperCase(), bean.getHotspot());
                }
                
                return new Gson().toJson(Hotspot);
		 }else if(filterindex == 2) {
            	Rollover = new HashMap<String,AccessType>();
                
                for(ProviderBean bean : allnetwork) {
                	Rollover.put(bean.getName().toUpperCase(), bean.getDatarollover());
                }
                
                return new Gson().toJson(Rollover);
		 }else if(filterindex == 3) {
            	Termination = new HashMap<String,AccessType>();
                
                for(ProviderBean bean : allnetwork) {
                	Termination.put(bean.getName().toUpperCase(), bean.getEarlytermination());
                }
                
                return new Gson().toJson(Termination);
		 }else {
            	International = new HashMap<String,Integer>();
                
                for(ProviderBean bean : allnetwork) {
                	International.put(bean.getName().toUpperCase(), bean.getInternational());
                }
                return new Gson().toJson(International);
        }
	}

}
