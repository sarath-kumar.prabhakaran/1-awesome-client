package com.network.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

@Path("/")
public class GetData {
	
	@GET
	@Path("/all")
	@Consumes(MediaType.APPLICATION_JSON)
	public String returnAllNetworks() {
		return new GenerateData().getNetworkJSON();
	}
	
	
	@GET
	@Path("/{provider}")
	@Consumes(MediaType.APPLICATION_JSON)
	public String returnNetwork(@PathParam("provider") String provider) {
		return new GenerateData().getNetwork(provider);
	}
	
	@GET
	@Path("/filter/{fil}")
	@Consumes(MediaType.APPLICATION_JSON)
	public String returnFilter(@PathParam("fil") int filter) {
		return new GenerateData().getFilter(filter);
	}
	
}
