package com.network.bdd;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class Networks {
	
	private Client client;
	private String baseURI;
	private String Provider;
	private String actual;
	
	public void findNetwork(String provider) {
		
		client = ClientBuilder.newClient();
		baseURI = "http://localhost:8081/NetworkProvider/network";
		
		Provider = provider;
	}
	
	public void customerClicks() {
		
		Builder builder = client.target(baseURI).path(Provider).request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		actual = response.readEntity(String.class);

	}
	
	public int checkDataAllowance() {
		
		JsonParser parser = new JsonParser();
		JsonObject obj = (JsonObject) parser.parse(actual);
		
		int expectedAllowance = obj.get("speed").getAsInt();
		
		return expectedAllowance;
	}
	
	public int checkMobileHotSpot() {
		
		JsonParser parser = new JsonParser();
		JsonObject obj = (JsonObject) parser.parse(actual);
		
		int expectedAllowance = obj.get("hotspot").getAsInt();
		
		return expectedAllowance;
	}

	public int checkInternationalPrice() {
		
		JsonParser parser = new JsonParser();
		JsonObject obj = (JsonObject) parser.parse(actual);
		
		int expectedPrice = obj.get("international").getAsInt();
		
		return expectedPrice;
	}

}
