
package com.network.providers;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "NetworkService", targetNamespace = "http://providers.network.com/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface NetworkService {


    /**
     * 
     * @return
     *     returns java.util.List<com.network.providers.Provider>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getAllItems", targetNamespace = "http://providers.network.com/", className = "com.network.providers.GetAllItems")
    @ResponseWrapper(localName = "getAllItemsResponse", targetNamespace = "http://providers.network.com/", className = "com.network.providers.GetAllItemsResponse")
    @Action(input = "http://providers.network.com/NetworkService/getAllItemsRequest", output = "http://providers.network.com/NetworkService/getAllItemsResponse")
    public List<Provider> getAllItems();

}
