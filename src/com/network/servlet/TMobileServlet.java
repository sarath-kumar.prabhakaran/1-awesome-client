package com.network.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.network.rest.ProviderBean;

/**
 * Servlet implementation class TMobileServlet
 */
@WebServlet("/TMobileServlet")
public class TMobileServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private Client client;
	private String baseURI;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TMobileServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		client = ClientBuilder.newClient();
		baseURI = "http://localhost:8081/NetworkProvider/network";
		
		Builder builder = client.target(baseURI).path("TMobile").request(MediaType.APPLICATION_JSON);
		Response resp = builder.buildGet().invoke();
		
		ProviderBean actual = resp.readEntity(ProviderBean.class);
		
		response.setContentType("text/html");
	    request.setAttribute("serverData", actual);
	    RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/tmobiledisplay.jsp");
	    		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
