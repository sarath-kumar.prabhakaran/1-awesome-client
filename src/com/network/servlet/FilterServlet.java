package com.network.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.network.rest.ProviderBean;

/**
 * Servlet implementation class FilterServlet
 */
@WebServlet("/FilterServlet")
public class FilterServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private Client client;
	private String baseURI;
	private String[] filters = {
			"High-Speed Data Allowance for Unlimited Plan(GB)",
			"Mobile Hotspot Allowance for Unlimited Plan(GB)",
			"Data Rollover",
			"Early Termination Assistance",
					"International Calling Plans($)"};
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FilterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		response.setContentType("text/html");
		request.setAttribute("filterData", filters);
		RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/displayfilters.jsp");
		dispatcher.forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String[] filter_selected;
		filter_selected = request.getParameterValues("filter_selected") ;
		
		Response resp;
		JsonArray data = new JsonArray();
		
		ArrayList<String> filter_display = new ArrayList<String>(filter_selected.length);
		
		for(String s:filter_selected) {
			int foo = Integer.parseInt(s);
			filter_display.add(filters[foo]);
		}
		
		
		client = ClientBuilder.newClient();
		baseURI = "http://localhost:8081/NetworkProvider/network/filter";
		
		for(int i=0;i<filter_selected.length;i++) {
			
			Builder builder = client.target(baseURI).path(filter_selected[i]).request(MediaType.APPLICATION_JSON);
			resp = builder.buildGet().invoke();
			String restfilterdata = resp.readEntity(String.class);
			
			JsonParser jsonParser = new JsonParser();
			JsonObject weatherdata = (JsonObject)jsonParser.parse(restfilterdata);
			
			//JsonObject row = new JsonObject();
			//row.add(filter_selected[i], weatherdata);
			
			data.add(weatherdata);
		}
		
		response.setContentType("text/html");
		request.setAttribute("filter_display", filter_display);
	    request.setAttribute("serverData", data);
	    RequestDispatcher dispatcher = request.getServletContext().getRequestDispatcher("/displayfilters.jsp");
	    		dispatcher.forward(request, response);
		
		
		
		
	}

}
