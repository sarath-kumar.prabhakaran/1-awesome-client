<%@page import="com.network.providers.Provider"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
   <%@ page import ="java.util.*" %>
   <%@ page import ="com.network.providers.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
img {
    width: 50%;
    height: auto;
    display: block;
    margin-left: auto;
    margin-right: auto;
    margin-top: 20px;
}
.button5 {background-color: #4CAF50;} 
html, body { height:100%; margin:0; padding:0 }
div { position:fixed; width:50%; height:50% }
#div1 { top:0;   left:0;   background:rgba(253, 238, 73, 0.5)  }
#div4 { top:0;   left:50%; background:rgba(46, 192, 245, 0.5)    }
#div3 { top:50%; left:0;   background:rgba(230, 30, 30, 0.5)  }
#div2 { top:50%; left:50%; background:rgba(242, 69, 156, 0.5)}  
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<title>Products</title>
</head>
<body>

<%
ArrayList<Provider> list = (ArrayList<Provider>) request.getAttribute("serverData");
//for (Provider c : list) {
%>
<div id="div1">
<img src="<%=list.get(0).getImage()%>" alt="Providers"/>
<center>
<h2><%=list.get(0).getDescription()%></h2>
<h3><%=list.get(0).getPrice()%></h3>
<form action="SprintServlet" method="get">
  <button class="btn btn-success" type="submit"> View Plan </button>
 </form>
</center>
</div>

<div id="div2">
<img src="<%=list.get(1).getImage()%>" alt="Providers"/>
<center>
<h2><%=list.get(1).getDescription()%></h2>
<h3><%=list.get(1).getPrice()%></h3>
<form action="TMobileServlet" method="get">
  <button class="btn btn-success" type="submit"> View Plan </button>
 </form>
</center>
</div>

<div id="div3">

<img src="<%=list.get(2).getImage()%>" alt="Providers"/>
<center>
<h2><%=list.get(2).getDescription()%></h2>
<h3><%=list.get(2).getPrice()%></h3>
<form action="VerizonServlet" method="get">
  <button class="btn btn-success" type="submit"> View Plan</button>
 </form>
</center>

</div>




<div id="div4">
<img src="<%=list.get(3).getImage()%>" alt="Providers"/>
<center>
<h2><%=list.get(3).getDescription()%></h2>
<h3><%=list.get(3).getPrice()%></h3>
<form action="AttServlet" method="get">
  <button class="btn btn-success" type="submit"> View Plan </button>
 </form>
</center>
</div>


<%       
//}
%>
<br>
</body>
</html>