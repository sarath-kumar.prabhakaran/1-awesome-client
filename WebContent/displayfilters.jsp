<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.google.gson.JsonParser"%>
<%@page import="com.google.gson.JsonElement"%>
<%@page import="com.google.gson.JsonObject"%>
<%@page import="com.google.gson.JsonArray"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="https://fonts.googleapis.com/css?family=Cinzel" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
    background-color: #FFFAF0;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr#get {
    background-color: rgba(0, 0, 0, 0);
}

table#t01 {    
    background-color: rgba(0, 0, 0, 0);
}

body {
font-family: "Cinzel";
background-color: black;
}

tr:nth-child(1) {
    background-color: rgba(200, 233, 32, 0.5); 
}
tr:nth-child(2) {
    background-color: rgba(88, 32, 25, 0.5); 
}
tr:nth-child(3) {
    background-color: rgba(253, 238, 73, 0.2); 
}
tr:nth-child(4) {
    background-color: rgba(242, 69, 156, 0.2); 
}
tr:nth-child(5) {
    background-color: rgba(230, 30, 30, 0.2); 
}
tr:nth-child(6) {
    background-color: rgba(46, 192, 245, 0.2); 
}
</style>
<title>Filter Criteria </title>
</head>
<body>

<% if (request.getAttribute("serverData") == null) { %> 

<div class="container">
  <div class="jumbotron">

<h2>Filter Options:</h2>

<form method="post" action="FilterServlet">

<table id="t01" border=2 cellpadding=2>
<tr id="get">
	<th>Filters Select</th>
    <th>Available Filters</th>
  </tr>
<%
String[] list = (String[]) request.getAttribute("filterData");
for(int i=0;i<list.length;i++){
%>
<tr id="get">
<td><input type="checkbox" name="filter_selected" value="<%= i %>"></td>
<td> <%= list[i] %></td>
</tr>

<%       
}
%>
</table>
<br>
<center><button class="btn btn-success" type="submit">Show Data</button></center>

</div>
</div>

<% } else {%> 

<%
//String[] selected = (String[]) request.getAttribute("filter_display");
JsonArray list_new = (JsonArray) request.getAttribute("serverData");
ArrayList<String> selected = (ArrayList<String>) request.getAttribute("filter_display");

JsonParser jsonParser = new JsonParser();
for(int i = 0; i<list_new.size(); i++){ 
JsonObject objele = (JsonObject)jsonParser.parse(list_new.get(i).toString());
%>

<div class="container">
  <div class="jumbotron">
<table border=2 cellpadding=2>
<tr>
<th><%= selected.get(i) %></th>
<th></th>
</tr>
<tr>
    <th>Provider Name</th>
    <th>Option</th>
</tr>

<% 
for(Iterator iterator = objele.keySet().iterator(); iterator.hasNext();) {
    String key = (String) iterator.next();
    String value = (String) objele.get(key).toString();
%>

<tr>
<td> <%= key %></td>
<td> <%= value %></td>
</tr>
<%       
} 
%>
</table>
<%
}
%>
</div>
</div>
<h3>
<center>
<form>
  <input class="btn btn-success" type="button" value="Change Filter" onclick="history.back()"></input>
</form>

<br>
<a href="index.html">
   <button class="btn btn-primary">Network Home Page</button>
</a>

</center>
</h3> 
<%  } %> 

</form>

</body>
</html>