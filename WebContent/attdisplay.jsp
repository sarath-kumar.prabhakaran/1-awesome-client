<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ page import ="java.util.*" %>
   <%@ page import ="com.network.rest.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<style>
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: rgba(46, 192, 245, 0.5);
}
</style>
<title>AT&T Plan Details</title>
</head>
<body>
<div class="container">
  <div class="jumbotron">

<h2>AT&T - REST </h2>

<!-- <form method="post" action="productServlet"> -->

<table>
<%
ProviderBean list = (ProviderBean) request.getAttribute("serverData");
%>
<tr>
<td>Network Provider Name</td>
<td> <%=list.getName().toUpperCase()%></td>
</tr>

<tr>
<td>Unlimited Talk & Text </td>
<td><%=list.getUnlimited()%> </td>
</tr>

<tr>
<td>High-Speed Data Allowance for Unlimited Plan(GB)</td>
<td> <%=list.getSpeed()%></td>
</tr>

<tr>
<td>Mobile Hotspot Allowance for Unlimited Plan(GB)</td>
<td><%=list.getHotspot()%> </td>
</tr>

<tr>
<td>Data Rollover</td>
<td> <%=list.getDatarollover()%></td>
</tr>

<tr>
<td>HD Video Streaming</td>
<td><%=list.getHdstreaming()%> </td>
</tr>

<tr>
<td>Early Termination Assistance</td>
<td> <%=list.getEarlytermination()%></td>
</tr>

<tr>
<td>Business Plans</td>
<td><%=list.getBusiness()%> </td>
</tr>

<tr>
<td>International Calling Plans Charge</td>
<td> <%=list.getInternational()%></td>
</tr>

</table>
<br>

<form>
  <input class="btn btn-success" type="button" value="Go Back" onclick="history.back()"></input>
</form>

<br>

<a href="index.html">
   <button class="btn btn-primary">Network Home Page</button>
</a>


<br>
</div>
</div>

</body>
</html>