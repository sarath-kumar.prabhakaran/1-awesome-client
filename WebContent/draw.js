window.onload = function() {
var ctx = document.getElementById("chart-area4");
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ["T-MOBILE", "AT&T", "SPRINT", "VERIZON"],
        datasets: [{
            label: '',
            data: [19, 13, 10, 16],
            backgroundColor: [
                'rgba(242, 69, 156, 0.1)',
                'rgba(46, 192, 245, 0.1)',
                'rgba(253, 238, 73, 0.1)',
                'rgba(230, 30, 30, 0.1)'
            ],
            borderColor: [
            	'rgba(220, 9, 185, 0.82)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(255,99,132,1)'  
            ],
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:true
                }
            }]
        }
    }
});
}