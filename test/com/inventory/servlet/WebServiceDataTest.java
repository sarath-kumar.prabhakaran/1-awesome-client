package com.inventory.servlet;

import static org.junit.Assert.*;

import java.util.List;

import javax.management.InstanceAlreadyExistsException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.network.providers.NetworkService;
import com.network.providers.NetworkServiceService;
import com.network.providers.Provider;

public class WebServiceDataTest {

	private NetworkService webservice;
	private List<Provider> PassedData;
	
	@Before
	public void setUp() throws Exception {
		
		webservice = new NetworkServiceService().getNetworkServicePort();
		PassedData = webservice.getAllItems();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void productsSize() {
		int actual = PassedData.size();
		int expected = 4;
		assertEquals(expected, actual);
	}

	@Test
	public void testdescriptionfirstproduct() {
		String actual = PassedData.get(0).getDescription();
		String expected = "Sprint Corporation";
		
		assertEquals(expected, actual);
	}
	@Test
	public void testpricefirstproduct() {
		
		double actual = PassedData.get(0).getPrice();
		double expected = 50.00;
		
		assertEquals(expected, actual,0.001);
	}

	@Test
	public void testdescriptionlastproduct() {
		String actual = PassedData.get(PassedData.size() - 1).getDescription();
		String expected = "AT&T";
		
		assertEquals(expected, actual);
	}
	@Test
	public void testpricelastproduct() {
		
		double actual = PassedData.get(PassedData.size() - 1).getPrice();
		double expected = 90.00;
		
		assertEquals(expected, actual,0.001);
	}
	
}
