Feature: Get Mobile Hotspot Allowance  
In the Single line - Unlimited Plans provided by Different Network get the Mobile Hotspot Allowance .

@mobile
Scenario Outline: Get High-Speed Data Allowance provided by Available Network 
	Given a network "<Provider>" Data plan
	When the customer clicks the More options
	Then the Website shows the Mobile hotspot <Allowance>

	Examples:
	| Provider	| Allowance |
	| verizon	|   10  |
	|  	 at&t	|   10  |
	|  sprint	|   10  |
	|  tmobile	|   10  |