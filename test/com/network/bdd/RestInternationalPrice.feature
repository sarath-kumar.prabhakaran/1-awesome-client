Feature: Get International Calling Options 
In the Single line - Unlimited Plans provided by Different Network get the Price of International Calling Plans.

@international
Scenario Outline: Get Price provided by Available Network 
	Given a network "<Provider>"
	When the customer clicks the Load More button
	Then the Website shows the <Price> of International Calling 

	Examples:
	| Provider	| Price |
	| verizon	|   15  |
	|  	 at&t	|   40  |
	|  sprint	|   15  |
	|  tmobile	|   15  |
	