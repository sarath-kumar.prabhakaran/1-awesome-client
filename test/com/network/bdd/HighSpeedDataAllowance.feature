Feature: Get High Speed Data Allowance  
In the Single line - Unlimited Plans provided by Different Network get the High-Speed Data Allowance.

@speed
Scenario Outline: Get High-Speed Data Allowance provided by Available Network 
	Given a network "<Provider>" plan
	When the customer clicks the Load More options
	Then the Website shows the <Allowance> of High-Speed Data

	Examples:
	| Provider	| Allowance |
	| verizon	|   22  |
	|  	 at&t	|   22  |
	|  sprint	|   23  |
	|  tmobile	|   30  |