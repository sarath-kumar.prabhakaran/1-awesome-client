package com.network.bdd;

import static org.junit.Assert.assertEquals;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class MobileHotSpotAllowance {
	
	Networks network = new Networks();
	
	@Given("^a network \"([^\"]*)\" Data plan$")
	public void a_network_Data_plan(String arg1) throws Throwable {
		network.findNetwork(arg1);
	}

	@When("^the customer clicks the More options$")
	public void the_customer_clicks_the_More_options() throws Throwable {
		network.customerClicks();
	}

	@Then("^the Website shows the Mobile hotspot (\\d+)$")
	public void the_Website_shows_the_Mobile_hotspot(int arg1) throws Throwable {
		
		int actual = arg1;
		int expected = network.checkMobileHotSpot();
		
		assertEquals(expected, actual);
		
	}
}
