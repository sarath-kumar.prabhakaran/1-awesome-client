package com.network.bdd;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.junit.Assert.assertEquals;

public class DataAllowanceTest {
	
	Networks network = new Networks();
	
	@Given("^a network \"([^\"]*)\" plan$")
	public void a_network_plan(String arg1) throws Throwable {
	    
		network.findNetwork(arg1);
	    
	}

	@When("^the customer clicks the Load More options$")
	public void the_customer_clicks_the_Load_More_options() throws Throwable {
		
		network.customerClicks();
	    
	}

	@Then("^the Website shows the (\\d+) of High-Speed Data$")
	public void the_Website_shows_the_of_High_Speed_Data(int arg1) throws Throwable {
	   
		int actual = arg1;
		int expected = network.checkDataAllowance();
		
		assertEquals(expected, actual);
	    
	}

}
