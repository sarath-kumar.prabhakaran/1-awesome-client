package com.network.bdd;

import static org.junit.Assert.assertEquals;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class InternationalTest {
	
	Networks network = new Networks();
	
	@Given("^a network \"([^\"]*)\"$")
	public void a_network(String arg1) throws Throwable {
	    
		network.findNetwork(arg1);
	 
	}

	@When("^the customer clicks the Load More button$")
	public void the_customer_clicks_the_Load_More_button() throws Throwable {
	    
		network.customerClicks();
	}

	@Then("^the Website shows the (\\d+) of International Calling$")
	public void the_Website_shows_the_of_International_Calling(int arg1) throws Throwable {
	    
		int actual = arg1;
		int expected = network.checkInternationalPrice();
		
		assertEquals(expected, actual);
	 
	}

}
