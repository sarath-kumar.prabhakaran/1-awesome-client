package com.network.rest;

import static org.junit.Assert.*;

import javax.swing.plaf.SliderUI;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class BadRequestTest {
	
	private Client client;
	private String baseURI;

	@Before
	public void setUp() throws Exception {
		
		client = ClientBuilder.newClient();
		baseURI = "http://localhost:8081/NetworkProvider/network";
	}

	@Test
	public void TestbadRequest() {
		
		Builder builder = client.target(baseURI).path("sweater").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		String actual = response.readEntity(String.class);
		String expected = "Sorry Bad Input";
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void TestRequestUpperCase() {
		
		Builder builder = client.target(baseURI).path("VERIZON").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		String actual = response.readEntity(String.class);
		String expected = "{\"name\":\"verizon\",\"unlimited\":\"AVAILABLE\",\"speed\":22,\"hotspot\":10,\"datarollover\":\"AVAILABLE\",\"hdstreaming\":\"AVAILABLE\",\"earlytermination\":\"AVAILABLE\",\"business\":\"AVAILABLE\",\"international\":15}";
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void TestRequestLowerCase() {
		
		Builder builder = client.target(baseURI).path("tmobile").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		String actual = response.readEntity(String.class);
		String expected = "{\"name\":\"tmobile\",\"unlimited\":\"AVAILABLE\",\"speed\":30,\"hotspot\":10,\"datarollover\":\"AVAILABLE\",\"hdstreaming\":\"AVAILABLE\",\"earlytermination\":\"NOTAVAILABLE\",\"business\":\"AVAILABLE\",\"international\":15}";
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void TestRequestCaseMixed() {
		
		Builder builder = client.target(baseURI).path("sPriNt").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		String actual = response.readEntity(String.class);
		String expected = "{\"name\":\"sprint\",\"unlimited\":\"AVAILABLE\",\"speed\":23,\"hotspot\":10,\"datarollover\":\"NOTAVAILABLE\",\"hdstreaming\":\"AVAILABLE\",\"earlytermination\":\"AVAILABLE\",\"business\":\"AVAILABLE\",\"international\":15}";
		
		assertEquals(expected, actual);
	}
	

}
