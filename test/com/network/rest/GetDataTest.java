package com.network.rest;

import static org.junit.Assert.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class GetDataTest {

	private Client client;
	private String baseURI;
	private String baseURIfilter;
	
	@Before
	public void setUp() throws Exception {
		
		client = ClientBuilder.newClient();
		baseURI = "http://localhost:8081/NetworkProvider/network";
		baseURIfilter = "http://localhost:8081/NetworkProvider/network/filter";
		
	}

	@Test
	public void TestVerizon() {

		Builder builder = client.target(baseURI).path("verizon").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		String actual = response.readEntity(String.class);
		String expected = "{\"name\":\"verizon\",\"unlimited\":\"AVAILABLE\",\"speed\":22,\"hotspot\":10,\"datarollover\":\"AVAILABLE\",\"hdstreaming\":\"AVAILABLE\",\"earlytermination\":\"AVAILABLE\",\"business\":\"AVAILABLE\",\"international\":15}";
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void TestATT() {
		
		Builder builder = client.target(baseURI).path("at&t").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		String actual = response.readEntity(String.class);
		String expected = "{\"name\":\"at\\u0026t\",\"unlimited\":\"AVAILABLE\",\"speed\":22,\"hotspot\":10,\"datarollover\":\"NOTAVAILABLE\",\"hdstreaming\":\"AVAILABLE\",\"earlytermination\":\"NOTAVAILABLE\",\"business\":\"AVAILABLE\",\"international\":40}";
	
		assertEquals(expected, actual);
	}
	
	@Test
	public void TestSprint() {
		
		Builder builder = client.target(baseURI).path("sprint").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		String actual = response.readEntity(String.class);
		String expected = "{\"name\":\"sprint\",\"unlimited\":\"AVAILABLE\",\"speed\":23,\"hotspot\":10,\"datarollover\":\"NOTAVAILABLE\",\"hdstreaming\":\"AVAILABLE\",\"earlytermination\":\"AVAILABLE\",\"business\":\"AVAILABLE\",\"international\":15}";
				
		assertEquals(expected, actual);
	}
	
	@Test
	public void TestTMobile() {
		
		Builder builder = client.target(baseURI).path("tmobile").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		String actual = response.readEntity(String.class);
		String expected = "{\"name\":\"tmobile\",\"unlimited\":\"AVAILABLE\",\"speed\":30,\"hotspot\":10,\"datarollover\":\"AVAILABLE\",\"hdstreaming\":\"AVAILABLE\",\"earlytermination\":\"NOTAVAILABLE\",\"business\":\"AVAILABLE\",\"international\":15}";
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void Testall() {
		Builder builder = client.target(baseURI).path("all").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		String jsonresponse = response.readEntity(String.class);
		
		JsonParser jsonParser = new JsonParser();
		JsonObject weatherdata = (JsonObject)jsonParser.parse(jsonresponse);
		
		assertEquals(4, weatherdata.size());
	}
	
	@Test
	public void TestfilterHighSpeed() {
		
		//String speed = "High-Speed Data Allowance for Unlimited Plan(GB)";
		
		String path = "0";

		Builder builder = client.target(baseURIfilter).path("0").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		String actual = response.readEntity(String.class);
		String expected = "{\"SPRINT\":23,\"TMOBILE\":30,\"VERIZON\":22,\"AT\\u0026T\":22}";
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void TestfilterMobileHotspot() {
		//String mobile = "Mobile Hotspot Allowance for Unlimited Plan(GB)";
		String path = "0";

		Builder builder = client.target(baseURIfilter).path("1").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		String actual = response.readEntity(String.class);
		String expected = "{\"SPRINT\":10,\"TMOBILE\":10,\"VERIZON\":10,\"AT\\u0026T\":10}";
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void TestfilterRollover() {
		
		//String data = "Data Rollover";

		Builder builder = client.target(baseURIfilter).path("2").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		String actual = response.readEntity(String.class);
		String expected = "{\"SPRINT\":\"NOTAVAILABLE\",\"TMOBILE\":\"AVAILABLE\",\"VERIZON\":\"AVAILABLE\",\"AT\\u0026T\":\"NOTAVAILABLE\"}";
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void TestTermination() {
		
		//String early = "Early Termination Assistance";

		Builder builder = client.target(baseURIfilter).path("3").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		String actual = response.readEntity(String.class);
		String expected = "{\"SPRINT\":\"AVAILABLE\",\"TMOBILE\":\"NOTAVAILABLE\",\"VERIZON\":\"AVAILABLE\",\"AT\\u0026T\":\"NOTAVAILABLE\"}";
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void TestInternational() {
		
		//String plans = "International Calling Plans";

		Builder builder = client.target(baseURIfilter).path("4").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		
		String actual = response.readEntity(String.class);
		String expected = "{\"SPRINT\":15,\"TMOBILE\":15,\"VERIZON\":15,\"AT\\u0026T\":40}";
		
		assertEquals(expected, actual);
	}

}
