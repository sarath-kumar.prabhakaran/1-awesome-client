package com.network.rest;

import static org.junit.Assert.*;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestNetworkInstances {
	
	private Client client;
	private String baseURI;

	@Before
	public void setUp() throws Exception {
		
		client = ClientBuilder.newClient();
		baseURI = "http://localhost:8081/NetworkProvider/network";
	}

	@Test
	public void Testinstanceverizon() {
		Builder builder = client.target(baseURI).path("verizon").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		ProviderBean actual = response.readEntity(ProviderBean.class);
		assertTrue(actual instanceof ProviderBean);
	}

	@Test
	public void Testinstanceatt() {
		Builder builder = client.target(baseURI).path("at&t").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		ProviderBean actual = response.readEntity(ProviderBean.class);
		assertTrue(actual instanceof ProviderBean);
	}
	
	@Test
	public void Testinstancesprint() {
		Builder builder = client.target(baseURI).path("sprint").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		ProviderBean actual = response.readEntity(ProviderBean.class);
		assertTrue(actual instanceof ProviderBean);
	}
	
	@Test
	public void Testinstancetmobile() {
		Builder builder = client.target(baseURI).path("tmobile").request(MediaType.APPLICATION_JSON);
		Response response = builder.buildGet().invoke();
		ProviderBean actual = response.readEntity(ProviderBean.class);
		assertTrue(actual instanceof ProviderBean);
	}
}
